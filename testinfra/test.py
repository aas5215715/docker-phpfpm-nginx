import pytest
import testinfra

def test_os_release(host):
  assert host.file("/etc/os-release").contains("Alpine Linux")

def test_packages(host):
  pkg = host.package("nginx")
  assert pkg.is_installed
  assert pkg.version.startswith("1.16")
